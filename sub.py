import cv2
import telebot
import os
import subprocess
import shutil
import time
import re
bot = telebot.TeleBot("1000118204:AAHQHI9DTwcYXNPjFXlORuoJL138c78hdqA")

def my_function(time):
    line = re.sub(':', '_', time)
    space = re.sub(" ",'_',line)

    file = open("time.txt", 'r+')
    timer = file.read()
    file.close()
    #Проверка есть ли папка с временем
    if os.path.exists(timer):
        print("Файл существует")
    else:
        print("Файл не существует")
        os.mkdir(space)
        file_w = open("time.txt", 'w')
        file_w.write(space)
        file_w.close()
    #Есть ли название папки в txt
    if os.stat("time.txt").st_size == 0:
        file_w = open("time.txt", 'w')
        file_w.write(space)
        file_w.close()

        file = open("time.txt", 'r+')
        p = file.read()
        os.mkdir(p)
        os.rename(p, space)
        file.close()
    else:
        file = open("time.txt", 'r+')
        p = file.read()

        os.rename(p, space)
        file.close()

    #заносим текущее время в txt
    file_w = open("time.txt", 'w')
    file_w.write(space)
    file_w.close()

my_function(time.asctime())

def create_time_folder():
    # Создается папка для времени
    file = open("time.txt", 'r+')
    timer = file.read()
    file.close()
    return timer

def translation_in_txt(message):
    file_info = bot.get_file(message.photo[len(message.photo) - 1].file_id)
    original = file_info.file_path
    removed = str(original).replace('photos/', '').replace('.jpg', '.txt')

    img = open('img.txt', 'a')
    img.writelines(removed)
    img.close()
    return removed

def outpu(timer,message):
    file_info = bot.get_file(message.photo[len(message.photo) - 1].file_id)
    # Сохраняем текст в папку outputs
    outputs = os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + '\\outputs'

    if not os.path.exists(outputs):
        os.mkdir(outputs)

    img = open("img.txt", 'r+')

    res = img.read()

    text = open(os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + '\\outputs\\' + res, "a")

    img.close()

    text.close()

    clear = open("img.txt", 'w')
    clear.close()

    runningProcess = subprocess.Popen("NNExperimental.exe " + os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + "\\" + file_info.file_path)
    runningProcess.wait()

    output = open("output.txt", 'r')

    lines1 = output.readlines()
    for l in lines1[1:]:
        bot.send_message(message.from_user.id, l)
    output.close()

def download_file(message,timer):
    file_info = bot.get_file(message.photo[len(message.photo) - 1].file_id)
    downloaded_file = bot.download_file(file_info.file_path)

    src = os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + "\\" + file_info.file_path
    with open(src, 'wb') as new_file:
        new_file.write(downloaded_file)

def create_photo_folder(timer,message):
    # Сохраняем картинки в папку photo
    photo = os.getcwd() + '\\' + timer + "\\" + message.from_user.first_name + '\\photos'

    if not os.path.exists(photo):
        os.mkdir(photo)

@bot.message_handler(content_types=['photo'])
def handle_photo(message):
    try:
        #Заносим id пользователей в user.txt
        user = str(message.from_user.id)
        user_file = open(r"C:\Release\user.txt","r+")
        text = user_file.read()

        #Проверка есть ли юзер в user.txt
        if str(user) in text:
            print("yes")

            timer = create_time_folder()

            removed = translation_in_txt(message)

            download_file(message,timer)

            outpu(timer,message)

            shutil.copy(r'output.txt',os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + '\\outputs\\' + removed)

        else:
            timer = create_time_folder()

            user_file.write(str(message.from_user.id) + "\n")
            user_file.close()
            os.mkdir(timer + "\\" + message.from_user.first_name)

            create_photo_folder(timer, message)

            removed = translation_in_txt(message)

            download_file(message, timer)

            outpu(timer, message)

            shutil.copy(r'output.txt',os.getcwd() + "\\" + timer + "\\" + message.from_user.first_name + '\\outputs\\' + removed)
    except Exception as e:
        bot.reply_to(message, e)

@bot.message_handler(commands=['help'])
def handle_start(message):
    bot.send_message(message.from_user.id,'Добро пожаловать...')





if __name__ == '__main__':
    bot.polling(none_stop=True)